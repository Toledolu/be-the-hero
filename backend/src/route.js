const express = require('express');
const { celebrate, Segments, Joi } = require('celebrate');

const ong_controller = require('./controllers/ong_controller');
const incident_controller = require('./controllers/incident_controller');
const profile_controller = require('./controllers/profile_controller');

const session_controller = require('./controllers/session_controller');

const routes = express.Router();

/**
 * Rota / Recurso
 * 
 * Métodos HTTP:
 * 
 * GET: Busca/Lista uma informação do backend
 * POST: Cria uma informação no backend
 * PUT/PATCH: Altera uma informação do backend
 * DELETE: Deletar uma informção do backend 
 * 
 * Tipos de Parâmetros:
 * 
 * Query: ParÂmetros nomeados enviados na rota após ? para filtros e paginação
 * Route: Parâmetros utilizados para identificar recursos
 * Request Body: Corpo da requisição para criar ou alterar recursos
 * 
 */

routes.get('/', (request, response) => {
  return response.json({
    evento: 'Hello World',
    aluno: 'Lucas'
  });
});

routes.post('/sessions', session_controller.create);

routes.get('/ongs', ong_controller.list);
routes.post('/ongs', celebrate({
  [Segments.BODY]: Joi.object().keys({
    name: Joi.string().required(),
    email: Joi.string().required().email(),
    whatsapp: Joi.string().required().min(10).max(11),
    city: Joi.string().required(),
    uf: Joi.string().required().length(2)
  })
}), ong_controller.create);

routes.get('/profile', celebrate({
  [Segments.HEADERS]: Joi.object({
    authorization: Joi.string().required()
  }).unknown()
}), profile_controller.list);

routes.get('/incidents', celebrate({
  [Segments.QUERY]: Joi.object().keys({
    page: Joi.number()
  })
}), incident_controller.list);
routes.post('/incidents', incident_controller.create);
routes.delete('/incidents/:id', celebrate({
  [Segments.PARAMS]: Joi.object().keys({
    id: Joi.number().required()
  })
}), ong_controller.delete);

module.exports = routes;