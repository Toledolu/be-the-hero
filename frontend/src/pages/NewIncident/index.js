import React, { useState } from 'react';
import { Link, useHistory } from 'react-router-dom';
import { FiArrowLeft } from 'react-icons/fi';
import api from '../../services/api';

import './styles.css';

import logo_img from '../../assets/logo.svg';

export default function NewIncident() {
  const [title, setTitle] = useState('');
  const [description, setDescription] = useState('');
  const [value, setValue] = useState('');

  const ongId = localStorage.getItem('ongId');
  const history = useHistory();

  async function handleNewIncident(e) {
    e.preventDefault();

    const data = {
      title,
      description,
      value
    };

    try {
      api.post('incidents', data, {
        headeers: {
          Authorization: ongId
        }
      });

      history.push('profile');
    } catch (error) {
      alert('Erro ao cadastrar')
    }
  }

  return (
    <div className="new-incident">
      <div className="content">
        <section>
          <img src={logo_img} alt="Be The Hero"/>

          <h1>Cadastrar novo Caso</h1>
          <p>Descreva o caso detalhadamente para encontrar um héroi para resolver isso.</p>

          <Link className="back-link" to="/profile">
            <FiArrowLeft size="16" color="#e02041"/>
            Voltar para Home
          </Link>
        </section>

        <form>
          <input type="text" placeholder="Título do caso" value={title} onChange={e => setTitle(e.target.title)}/>
          <textarea type="text" name="description" placeholder="Descricao" value={description} onChange={e => setDescription(e.target.description)}></textarea>
          <input type="text" placeholder="Valor em Reais" value={value} onChange={e => setValue(e.target.value)}/>

          <button className="button" type="submit">Cadastrar</button>
        </form>
      </div>
    </div>
  );
}