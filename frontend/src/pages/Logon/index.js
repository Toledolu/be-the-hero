import React, { useState } from 'react';
import { Link, useHistory } from 'react-router-dom';
import { FiLogIn } from 'react-icons/fi'
import api from '../../services/api';

import './styles.css';

import logo_img  from "../../assets/logo.svg";
import heroes_img from "../../assets/heroes.png";

export default function Logon() {
  const [id, setId] = useState('');
  const history = useHistory();

  async function handleLogin(e) {
    e.preventDefault();

    try {
      const response = await api.post('sessions', {id});

      localStorage.setItem('ongId', id);
      localStorage.setItem('ongName', response.data.name);

      history.push('/profile');
    } catch (error) {
      alert("Erro no login");
    }
  }

  return(
    <div className="logon-container">
      <section className="form">
        <img src={logo_img} alt="Be The Hero"/>

        <form onSubmit={handleLogin}>
          <h1>Faça seu logon</h1>
          <input placeholder="Sua ID" value={id} onChange={e => setId(e.target.value)}/>
          <button className="button" type="submit">Entrar</button>
        </form>

        <Link to="/register">
          <FiLogIn size="16" color="#e02041"/>
          Não tenho cadastro
        </Link>
      </section>

      <img src={heroes_img} alt="Heroes"/>
    </div>  
  );
}