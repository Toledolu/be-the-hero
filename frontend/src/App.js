import React from 'react';
//import Header from './Header';
import Routes from './routes';
import './app.css';

//JSX is when there is HTML(XML) inside JS
function App() {
  return (
    /*
    title="Semana Omnistack"
    <Header>
      Semana Omnistack
    </Header>
    */

    <Routes/>
  );
}

export default App;
